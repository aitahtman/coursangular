import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Posts';

  posts = [
    {
      title: 'Mon premier post',
      content: 'Itaque verae amicitiae difficillime reperiuntur in iis qui in honoribus reque publica versantur; ubi enim istum invenias  ',
      lovesIt: 4,
      created_at: '2017-01-01 23:34'
    },
    {
      title: 'Mon deuxième post',
      content: 'Itaque verae amicitiae difficillime reperiuntur in iis qui in honoribus reque publica versantur; ubi enim istum invenias ',
      lovesIt: 4,
      created_at: '2015-01-01 08:14'
    },
    {
      title: 'Encore un post',
      content: 'Itaque verae amicitiae difficillime reperiuntur in iis qui in honoribus reque publica versantur; ubi enim istum invenias ',
      lovesIt: 4,
      created_at: '2013-04-01 7:14:56'
    }
  ];
}
